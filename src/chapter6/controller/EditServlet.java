package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		Message message = getMessage(request);
//		int id = Integer.parseInt(request.getParameter("id"));
//		String messageText = request.getParameter("text");
		if (isValid(message, errorMessages)) {
			new MessageService().update(message);
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("messagetext", message);
			request.getRequestDispatcher("/edit.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("./");
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}


	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));
		return message;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		//int型のtextをtop.jspから取得、Message型に詰め、MessageService()selectの呼び出し 引数text
		String id = request.getParameter("messageText");
		List<String> errorMessages = new ArrayList<String>();

		if (StringUtils.isBlank(id) || !id.matches("^[0-9]*$") ) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("top.jsp").forward(request, response);
			return;
		}
		System.out.println(id);
		int text = Integer.parseInt(request.getParameter("messageText"));
		Message messages = new MessageService().select(text);

		User loginUser = (User) session.getAttribute("loginUser");
		User user = new UserService().select(loginUser.getId());
		Message searchId = new UserService().searchId(text);
		//	System.out.println("ラスト"+searchId);
		if (searchId == null) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("top.jsp").forward(request, response);
			return;
		}

		request.setAttribute("user", user);
		//値が戻ってきたらrequest.setAttribute("messagetext", messages);とbeansで宣言したtext←これ変えると？を参照し表示
		request.setAttribute("messagetext", messages);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);

	}

}
