package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {



    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            Date dateObj = new Date();
            SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

            String past;
    		if(!StringUtils.isEmpty(start)) {
    			past = start + " 00:00:00";
    		} else {
    			past =("2020-01-01 00:00:00");
    		}
    		String now;
    		if(!StringUtils.isEmpty(end)) {
    			now = end + " 23:59:59";
    		} else {
    			 now = format.format( dateObj );
    		}

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, now, past);
            commit(connection);
            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(int text) {

        Connection connection = null;

        try {
        	//DBと接続する
            connection = getConnection();
//MessageDao().selectの呼び出し 引数(connection, text)
            Message messages = new MessageDao().select(connection, text);
            commit(connection);
//messages（リスト）をEditServletへ戻す
            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {

        Connection connection = null;
        try {
        	 connection = getConnection();
             new MessageDao().update(connection, message);
             commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int text) {

        Connection connection = null;
        try {
        	 connection = getConnection();
             new MessageDao().delete(connection, text);
             commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }




}