package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("    user_id, ");
			sql.append("    text, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, "); // user_id
			sql.append("    ?, "); // text
			sql.append("    CURRENT_TIMESTAMP, "); // created_date
			sql.append("    CURRENT_TIMESTAMP "); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> select(Connection connection, Integer id, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    messages.text as text, ");
			sql.append("    messages.user_id as user_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE id = ? ");

			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection,Message message) {

		PreparedStatement ps = null;
		try {
			String sql = "UPDATE messages SET text = ? WHERE id = ? ";

			ps = connection.prepareStatement(sql);

			ps.setString(1, message.getText());
			ps.setInt(2, message.getId());

			ps.executeUpdate();

			int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setAccount(rs.getString("account"));
				message.setName(rs.getString("name"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}

	public Message select(Connection connection, int text) {
		PreparedStatement ps = null;
		//sql文作成''はいらない
		String sql = "SELECT * FROM messages WHERE id = ? ";
		try {
			//toString外すと？
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, text);
			//sql発行
			ResultSet rs = ps.executeQuery();
			//toMessagesメソッドを使って表からデータの取得
			Message messages = toMessages(rs);
			return messages;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private Message toMessages(ResultSet rs) throws SQLException {

		Message messages = new Message();
		try {
			while (rs.next()) {
				messages.setId(rs.getInt("id"));
				messages.setText(rs.getString("text"));
				messages.setUserId(rs.getInt("user_id"));
				messages.setCreatedDate(rs.getTimestamp("created_date"));
				messages.setUpdatedDate(rs.getTimestamp("updated_date"));
			}
			return messages;
		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, int text) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM messages WHERE id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, text);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}