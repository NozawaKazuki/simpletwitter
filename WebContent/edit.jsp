<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>編集画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

	<div class="form-area">
		<h1 style="text-align: right">
			<font size='3'>
			<a href="./">ホーム</a>&emsp; <a href="setting">設定</a>&emsp;
				<a href="logout">ログアウト</a>
			</font>
		</h1>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="edit" method="post">
			<input name="id" value="${messagetext.id}" id="massegeId" type="hidden" />
				<br />
				<br />
				&emsp;いま、どうしてる？
				<br />
			<textarea name="text" cols="50" rows="5" id="text"><c:out value="${messagetext.text}"/></textarea>
			<br />
			<input type="submit" value="更新">（140文字まで）
			<br />
		</form>
	</div>

	<div class="copyright">Copyright(c)NOZAWA KAZUKI</div>




</body>
</html>